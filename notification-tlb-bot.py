import requests
import os

def send_telegram(text: str):
    token = "os.getenv('TOKEN')"
    url = "https://api.telegram.org/bot"
    channel_id = "os.getenv('CHAT_ID')"
    url += token
    method = url + "/sendMessage"

    r = requests.post(method, data={
         "chat_id": channel_id,
         "text": text
          })

    if r.status_code != 200:
        raise Exception("post_text error")

if __name__ == '__main__':
  send_telegram ('Branch: ' + str(os.getenv('CI_COMMIT_REF_NAME')) + '\n' + \
                 'COMMIT SHORT SHA: ' + str(os.getenv('CI_COMMIT_SHORT_SHA')) + '\n' + \
                 'PROJECT URL: ' + str(os.getenv('CI_PROJECT_URL')) + '\n' + \
                 '✅ Pipeline successfully completed!')
                 
